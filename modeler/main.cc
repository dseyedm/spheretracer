#include <bits/stdc++.h>
using namespace std;
int main(int argc, char* argv[]) {
	if(argc < 2) {
		fprintf(stderr, "usage: %s <.obj>\n", argv[0]);
		return 1;
	}
	fstream fin; fin.open(argv[1]); assert(fin.is_open());
	string ln;
	while(getline(fin, ln)) {
		stringstream ss;
		ss << ln;
		string header;
		ss >> header;
		if(header != "v") continue;
		float a, b, c;
		ss >> a >> b >> c; 
		printf("%f %f %f\n", a, b, c);
	}
	return 0;
}
