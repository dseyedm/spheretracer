bool USE_BVH = true;
bool USE_BVH_SAH = true;
bool USE_BVH_OBJ = true;
bool USE_HEAT = false;

#define LINE printf("%d\n", __LINE__)
#define SIZE(a) (sizeof(a)/sizeof(*a))
#include "scope.h"
#include "vec.h"

#include <sys/time.h>
#include <pthread.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include <sstream>
#include <fstream>

using namespace std;

template<typename T> string to_str(T t) { stringstream ss; ss << t; return ss.str(); }

const double PI = acos(-1.0);
inline double rad(double deg)        { return deg*PI/180.0; }
inline double deg(double rad)        { return rad*180.0/PI; }
inline double frand()                { return (double)rand()/RAND_MAX; }
inline v3 tov3(int r, int g, int b) { return v3(r, g, b)/255.0; }
inline v3 random_in_unit_sphere() {
	v3 p; do { p = 2.0*v3(frand(), frand(), frand()) - v3(1.0); } while(p.len2() >= 1.0);
	return p;
}
inline v3 random_in_unit_disk() {
	v3 p; do { p = 2.0*v3(frand(), frand(), 0.0) - v3(1.0, 1.0, 0.0); } while(dot(p,p) >= 1.0);
	return p;
}
inline v3 reflect(const v3& v, const v3& n) {
	return v - 2.0*dot(v, n)*n;
}
inline bool refract(const v3& v, const v3& n, double ni_over_nt, v3& refracted) {
	v3 uv = v.unit();
	double dt = dot(uv, n);
	double discriminant = 1.0 - ni_over_nt*ni_over_nt*(1.0 - dt*dt);
	if(discriminant > 0.0) {
		refracted = ni_over_nt*(uv - n*dt) - n*sqrt(discriminant);
		return true;
	}
	return false;
}
inline double schlick(double cosine, double ref_idx) {
	double r0 = (1.0 - ref_idx)/(1.0 + ref_idx);
	r0 = r0*r0;
	return r0 + (1.0 - r0)*powf((1.0 - cosine), 5.0);
}

struct ray_t {
	v3 origin, dir;
	inline v3 along(double t) const { return origin + t*dir; }
};

struct aabb_t {
	v3 lo, hi;
	inline bool hit(const ray_t& r, double t_min, double t_max) const {
		for(int a = 0; a < 3; ++a) {
			double s0 = (lo[a] - r.origin[a])/r.dir[a];
			double s1 = (hi[a] - r.origin[a])/r.dir[a];
			double s_min, s_max;
			if(s0 < s1) { s_min = s0; s_max = s1; }
			else        { s_min = s1; s_max = s0; }
			t_min = fmaxf(s_min, t_min);
			t_max = fminf(s_max, t_max);
			if(t_max <= t_min) return false;
		}
		return true;
	}
	inline double area() const {
		double a = hi.x - lo.x;
		double b = hi.y - lo.y;
		double c = hi.z - lo.z;
		return 2*(a*b + b*c + c*a);
	}
	inline int longest_axis() const {
	       double a = hi.x - lo.x;
	       double b = hi.y - lo.y;
	       double c = hi.z - lo.z;
	       if(a > b && a > c) return 0;
	       if(b > c) return 1;
	       return 2;
	}
	static inline aabb_t parent(const aabb_t& b0, const aabb_t& b1) {
		aabb_t p;
		p.lo.x = fminf(b0.lo.x, b1.lo.x);
		p.lo.y = fminf(b0.lo.y, b1.lo.y);
		p.lo.z = fminf(b0.lo.z, b1.lo.z);
		p.hi.x = fmaxf(b0.hi.x, b1.hi.x);
		p.hi.y = fmaxf(b0.hi.y, b1.hi.y);
		p.hi.z = fmaxf(b0.hi.z, b1.hi.z);
		return p;
	}
};

// packing material_t into one type instead of using virtual*
// may speed up processing because of less pointer lookups
enum {
	MAT_MATTE,
	MAT_METAL,
	MAT_TRANSPARENT
};
struct material_t;
// a hit record, contains important information about how the object was hit for
// lighting purposes
struct hit_t {
	double t = 0; // where along the ray was the object hit?
	v3 p;        // what point in WS is the object hit?
	v3 normal;
	material_t* material = NULL; // what is the scene index for this object?
};
struct material_t {
	uint32_t type = 0; // MAT_*
	v3 albedo;     // MAT_MATTE or MAT_METAL
	union {
		double fuzz;       // MAT_METAL
		double refraction; // MAT_TRANSPARENT
	};

	bool scatter(const ray_t& in_ray, const hit_t& in_hit, v3* out_attenuation, ray_t* out_scattered) const {
		switch(type) {
			case MAT_MATTE: {
				v3 target = in_hit.p + in_hit.normal + random_in_unit_sphere();
				out_scattered->origin = in_hit.p;
				out_scattered->dir = target - in_hit.p;
				*out_attenuation = albedo;
				return true;
			} break;
			case MAT_METAL: {
				v3 reflected = reflect(in_ray.dir.unit(), in_hit.normal);
				out_scattered->origin = in_hit.p;
				out_scattered->dir = reflected + fuzz*random_in_unit_sphere();
				*out_attenuation = albedo;
				return dot(out_scattered->dir, in_hit.normal) > 0.0;
			} break;
			case MAT_TRANSPARENT: {
				*out_attenuation = v3(1.0);
				v3 outward_normal;
				v3 reflected = reflect(in_ray.dir, in_hit.normal);
				v3 refracted;
				double ni_over_nt, reflect_prob, cosine;
				if(dot(in_ray.dir, in_hit.normal) > 0.0) {
					outward_normal = -in_hit.normal;
					ni_over_nt = refraction;
					cosine = refraction*dot(in_ray.dir, in_hit.normal)/in_ray.dir.len();
				} else {
					outward_normal = in_hit.normal;
					ni_over_nt = 1.0/refraction;
					cosine = -dot(in_ray.dir, in_hit.normal)/in_ray.dir.len();
				}
				if(refract(in_ray.dir, outward_normal, ni_over_nt, refracted)) {
					reflect_prob = schlick(cosine, refraction);
				} else {
					out_scattered->origin = in_hit.p;
					out_scattered->dir = reflected;
					reflect_prob = 1.0;
				}
				if(frand() < reflect_prob) {
					out_scattered->origin = in_hit.p;
					out_scattered->dir = reflected;
				} else {
					out_scattered->origin = in_hit.p; 
					out_scattered->dir = refracted;
				}
				return true;
			} break;
		}
		assert(0);
	}
};


// since the general case for sphere_t is looking up center/radius, we don't 
// also pack material_t in there for better memory usage. it's a separate array.
struct sphere_t {
	v3 center;
	double radius = 0;
	material_t material;
	// note: does NOT set out_hit->scene_i.
	inline bool hit(const ray_t& in_ray, double in_t_min, double in_t_max, hit_t* out_hit) {
		v3 oc = in_ray.origin - center;
		double a = dot(in_ray.dir, in_ray.dir);
		double b = dot(oc, in_ray.dir);
		double c = dot(oc, oc) - radius*radius;
		double discriminant = b*b - a*c;
		if(discriminant <= 0.0) return false;
		double t = (-b - sqrtf(b*b - a*c))/a;
		if(in_t_min < t && t < in_t_max) {
			out_hit->t = t;
			out_hit->p = in_ray.along(t);
			out_hit->normal = (out_hit->p - center)/radius;
			out_hit->material = &material;
			return true;
		}
		t = (-b + sqrtf(b*b - a*c))/a;
		if(in_t_min < t && t < in_t_max) {
			out_hit->t = t;
			out_hit->p = in_ray.along(t);
			out_hit->normal = (out_hit->p - center)/radius;
			out_hit->material = &material;
			return true;
		}
		return false;
	}
	inline aabb_t get_box() const {
		aabb_t b;
		b.lo.x = center.x - radius;
		b.lo.y = center.y - radius;
		b.lo.z = center.z - radius;
		b.hi.x = center.x + radius;
		b.hi.y = center.y + radius;
		b.hi.z = center.z + radius;
		return b;
	}
};
inline sphere_t get_sphere_matte(v3 origin, double radius, v3 albedo) {
	sphere_t sphere;
	sphere.center = origin;
	sphere.radius = radius;
	sphere.material.type = MAT_MATTE;
	sphere.material.albedo = albedo;
	return sphere;
}
inline sphere_t get_sphere_metal(v3 origin, double radius, v3 albedo, double fuzz) {
	sphere_t sphere;
	sphere.center = origin;
	sphere.radius = radius;
	sphere.material.type = MAT_METAL;
	sphere.material.albedo = albedo;
	sphere.material.fuzz = fuzz;
	return sphere;
}
inline sphere_t get_sphere_transparent(v3 origin, double radius, double refraction) {
	sphere_t sphere;
	sphere.center = origin;
	sphere.radius = radius;
	sphere.material.type = MAT_TRANSPARENT;
	sphere.material.refraction = refraction;
	return sphere;
}

struct bvh_node_t {
	sphere_t* sphere = NULL; // non NULL if terminal
	bvh_node_t* left = NULL;
	bvh_node_t* right = NULL;
	aabb_t box; // ALWAYS set, even if sphere is non NULL.
};

struct scene_t {
	sphere_t* spheres = NULL;
	int n = 0;

	inline bool hit_linear(const ray_t& in_ray, double in_t_min, double in_t_max, hit_t* out_hit) const {
		bool did_hit = false;
		double closest_t = in_t_max;
		hit_t h;
		for(int i = 0; i < n; ++i) {
			if(!spheres[i].hit(in_ray, in_t_min, closest_t, &h)) continue;
			closest_t = h.t;
			did_hit = true;
		}
		if(!did_hit) return false;
		*out_hit = h;
		return true;
	}
	bool hit_bvh(const bvh_node_t* node, const ray_t& in_ray, double in_t_min, double in_t_max, hit_t* out_hit) const {
		// terminal node
//		if(!node) return false;
		if(node->sphere) {
			return node->sphere->hit(in_ray, in_t_min, in_t_max, out_hit);
		}

		if(!node->box.hit(in_ray, in_t_min, in_t_max)) return false;
		hit_t hit_left;
		bool did_hit_left = hit_bvh(node->left, in_ray, in_t_min, in_t_max, &hit_left);
		hit_t hit_right;
		bool did_hit_right = hit_bvh(node->right, in_ray, in_t_min, in_t_max, &hit_right);
		// tie break based on closer t value
		if(did_hit_left && did_hit_right) {
			if(hit_left.t < hit_right.t) *out_hit = hit_left;
			else                         *out_hit = hit_right;
			return true;
		}
		if(did_hit_left) {
			*out_hit = hit_left;
			return true;
		}
		if(did_hit_right) {
			*out_hit = hit_right;
			return true;
		}
		return false;
	}
};

int sphere_x_cmp(const void* a, const void* b) {
	// this can be simplified greatly
	sphere_t* sa = *(sphere_t**)a;
	sphere_t* sb = *(sphere_t**)b;
	aabb_t ba = sa->get_box();
	aabb_t bb = sb->get_box();
	if(ba.lo.x < bb.lo.x) return -1;
	return 1;
}
int sphere_y_cmp(const void* a, const void* b) {
	// this can be simplified greatly
	sphere_t* sa = *(sphere_t**)a;
	sphere_t* sb = *(sphere_t**)b;
	aabb_t ba = sa->get_box();
	aabb_t bb = sb->get_box();
	if(ba.lo.y < bb.lo.y) return -1;
	return 1;
}
int sphere_z_cmp(const void* a, const void* b) {
	// this can be simplified greatly
	sphere_t* sa = *(sphere_t**)a;
	sphere_t* sb = *(sphere_t**)b;
	aabb_t ba = sa->get_box();
	aabb_t bb = sb->get_box();
	if(ba.lo.z < bb.lo.z) return -1;
	return 1;
}
void build_bvh(bvh_node_t* node, sphere_t** spheres, int n) {
	assert(n > 0);
	node->sphere = NULL;
	node->box = spheres[0]->get_box();
	for(int i = 1; i < n; ++i) {
		aabb_t sphere_box = spheres[i]->get_box();
		node->box = aabb_t::parent(node->box, sphere_box);
	}

	int longest_axis = node->box.longest_axis();
	if     (longest_axis == 0) qsort(spheres, n, sizeof(*spheres), sphere_x_cmp);
	else if(longest_axis == 1) qsort(spheres, n, sizeof(*spheres), sphere_y_cmp);
	else                       qsort(spheres, n, sizeof(*spheres), sphere_z_cmp);

if(USE_BVH_SAH) {
	int min_sa_i = 0; {
		aabb_t* boxes       = (aabb_t*)malloc(sizeof(*boxes)*n);
		double* left_areas  = (double*)malloc(sizeof(*left_areas)*n);
		double* right_areas = (double*)malloc(sizeof(*right_areas)*n);

		for(int i = 0; i < n; ++i) {
			boxes[i] = spheres[i]->get_box();
		}

		aabb_t left_box = boxes[0];
		left_areas[0] = left_box.area();
		for(int i = 1; i + 1 < n; ++i) {
			left_box = aabb_t::parent(left_box, boxes[i]);
			left_areas[i] = left_box.area();
		}

		aabb_t right_box = boxes[n - 1];
		right_areas[n - 1] = right_box.area();
		for(int i = n - 2; i > 0; --i) {
			right_box = aabb_t::parent(right_box, boxes[i]);
			right_areas[i] = right_box.area();
		}

		double min_sa = 99999999.0;
		for(int i = 0; i + 1 < n; ++i) {
			double sa = i*left_areas[i] + (n - i - 1)*right_areas[i + 1];
			if(sa < min_sa) {
				min_sa = sa;
				min_sa_i = i;
			}
		}

		// free memory before recursion
		free(boxes);
		free(left_areas);
		free(right_areas);
	}

	node->right = (bvh_node_t*)malloc(sizeof(*node->right));
	node->left  = (bvh_node_t*)malloc(sizeof(*node->left));
	if(min_sa_i <= 0) {
		node->left->sphere = spheres[0];
		node->left->box = spheres[0]->get_box();
	} else {
		build_bvh(node->left, spheres, min_sa_i + 1);
	}
	if(min_sa_i + 2 >= n) {
		node->right->sphere = spheres[n - 1];
		node->right->box = spheres[n - 1]->get_box();
	} else {
		build_bvh(node->right, spheres + min_sa_i + 1, n - min_sa_i - 1);
	}
} else {
	node->right = (bvh_node_t*)malloc(sizeof(*node->right));
	node->left  = (bvh_node_t*)malloc(sizeof(*node->left));
	if(n == 1) {
		node->right->sphere = node->left->sphere = spheres[0];
		node->right->box = node->left->box = spheres[0]->get_box();
	} else if(n == 2) {
		node->left->sphere = spheres[0];
		node->left->box = spheres[0]->get_box();
		node->right->sphere = spheres[1];
		node->right->box = spheres[1]->get_box();
	} else {
		build_bvh(node->left, spheres, n/2);
		build_bvh(node->right, spheres + n/2, n - n/2);
	}
}
}
void free_bvh(bvh_node_t* root) {
	if(!root) return;
	if(!root->sphere) {
		free_bvh(root->right);
		free_bvh(root->left);
	}
	free(root);
}

void write_bvh_obj(bvh_node_t* root, fstream& fs, int& nv) {
	if(!root) return;
	static const char* e = " ";
	static const char* v = "v ";
	static const char* f = "f ";
	fs << v << root->box.lo.x << e << root->box.lo.y << e << root->box.lo.z << "\n";
	fs << v << root->box.lo.x << e << root->box.hi.y << e << root->box.lo.z << "\n";
	fs << v << root->box.hi.x << e << root->box.hi.y << e << root->box.lo.z << "\n";
	fs << v << root->box.hi.x << e << root->box.lo.y << e << root->box.lo.z << "\n";
	fs << v << root->box.lo.x << e << root->box.lo.y << e << root->box.hi.z << "\n";
	fs << v << root->box.lo.x << e << root->box.hi.y << e << root->box.hi.z << "\n";
	fs << v << root->box.hi.x << e << root->box.hi.y << e << root->box.hi.z << "\n";
	fs << v << root->box.hi.x << e << root->box.lo.y << e << root->box.hi.z << "\n";
	fs << f << nv+0 << e << nv+1 << e << nv+2 << e << nv+3 << "\n"; // XY
	fs << f << nv+0 << e << nv+4 << e << nv+5 << e << nv+1 << "\n"; // YZ
	fs << f << nv+0 << e << nv+4 << e << nv+7 << e << nv+3 << "\n"; // XZ
	fs << f << nv+6 << e << nv+5 << e << nv+1 << e << nv+2 << "\n"; // top
	fs << f << nv+6 << e << nv+2 << e << nv+3 << e << nv+7 << "\n"; // right
	fs << f << nv+6 << e << nv+5 << e << nv+4 << e << nv+7 << "\n"; // back
	nv += 8;
	if(!root->sphere) {
		write_bvh_obj(root->right, fs, nv);
		write_bvh_obj(root->left, fs, nv);
	}
}

struct camera_t {
	v3 origin;
	v3 lower_left_corner;
	v3 horizontal;
	v3 vertical;
	v3 u, v, w;
	double lens_radius = 0.0;
	camera_t() { }
	camera_t(v3 look_from, v3 look_at, v3 vup, double vfov, double aspect, double aperture, double focus_dist) {
		lens_radius = aperture/2.0;
		double theta = vfov;
		double half_height = tanf(theta/2.0);
		double half_width = aspect*half_height;
		origin = look_from;
		w = (look_from - look_at).unit();
		u = cross(vup, w).unit();
		v = cross(w, u);
		lower_left_corner = origin - half_width*focus_dist*u - half_height*focus_dist*v - focus_dist*w;
		horizontal = 2.0*half_width*focus_dist*u;
		vertical = 2.0*half_height*focus_dist*v;
	}
	inline ray_t get_ray(double s, double t) {
		v3 rd = lens_radius*random_in_unit_disk();
		v3 offset = u*rd.x + v*rd.y;
		ray_t r;
		r.origin = origin + offset;
		r.dir = lower_left_corner + s*horizontal + t*vertical - origin - offset;
		return r;
	}
};

inline v3 color(bvh_node_t* root, const scene_t& scene, const ray_t& r, int depth = 0, int max_depth = 40) {
	// scene
	hit_t hit;
	if((root && scene.hit_bvh(root, r, 0.001, 99999999.0, &hit)) || 
	  (!root && scene.hit_linear   (r, 0.001, 99999999.0, &hit))) {
		ray_t scattered;
		v3 attenuation;
		if(depth < max_depth && hit.material->scatter(r, hit, &attenuation, &scattered)) {
			return attenuation*color(root, scene, scattered, depth + 1, max_depth);
		}
		return v3(0.0);
	}

	// sky
	v3 unit_dir = r.dir.unit();
	double t = 0.5f*(unit_dir.y + 1.0);
	return lerp(v3(1.0), t, v3(0.5, 0.7f, 1.0));
}

struct render_settings_t {
	int n_threads = 1;
	camera_t cam;
	scene_t scene;
	bvh_node_t* root = NULL; // can be NULL
	int n_samples = 0;
	int width = 0, height = 0;
	vec3<uint8_t>* out_image = NULL;
	double* out_heat = NULL;

	render_settings_t() { }
};
struct thread_settings_t {
	render_settings_t* rs = NULL;
	int thread_i = 0;
	int start_height = 0;
	int end_height = 0; // not inclusive

	thread_settings_t() { }
};
static int pxcounts[128] = { 0 };
void* render_thread(void* ptr) {
	thread_settings_t& ts = *(thread_settings_t*)(ptr);
	pxcounts[ts.thread_i] = 0;
	int pxcount = 0;
	for(int h = ts.start_height; h < ts.end_height; ++h) {
		for(int w = 0; w < ts.rs->width; ++w) {
			double t0 = 0.0;
			if(USE_HEAT) {
				struct timeval tm; gettimeofday(&tm, NULL);
				t0 = (double)tm.tv_sec + (double)tm.tv_usec/1000000.0;
			}
			v3 col(0.0);
			for(int s = 0; s < ts.rs->n_samples; ++s) {
				double u = (double)(w + frand())/(double)ts.rs->width;
				double v = (double)(h + frand())/(double)ts.rs->height;
				ray_t r = ts.rs->cam.get_ray(u, v);
				col += color(ts.rs->root, ts.rs->scene, r);
			}
			col /= (double)ts.rs->n_samples;
			// gamma correction
			col.r = pow(col.r, 1.0/2.2);
			col.g = pow(col.g, 1.0/2.2);
			col.b = pow(col.b, 1.0/2.2);
			vec3<uint8_t>* px = &ts.rs->out_image[h*ts.rs->width + w];
			px->r = (int)(255.99f*col.r);
			px->g = (int)(255.99f*col.g);
			px->b = (int)(255.99f*col.b);

			if(USE_HEAT) {
				double t1;
				struct timeval tm; gettimeofday(&tm, NULL);
				t1 = (double)tm.tv_sec + (double)tm.tv_usec/1000000.0;

				ts.rs->out_heat[h*ts.rs->width + w] = t1 - t0;
			}
			++pxcount;
			if(pxcount % 100 == 0) pxcounts[ts.thread_i] = pxcount;
		}
	}
	pxcounts[ts.thread_i] = pxcount;
	return NULL;
}
void render(render_settings_t& rs) {
	assert(rs.n_threads >= 1 && rs.n_threads <= 128);

	pthread_t thread_handles[rs.n_threads];

	thread_settings_t thread_data[rs.n_threads];
	int height_per_thread = (rs.height + rs.n_threads - 1)/rs.n_threads; // rounded up
	int curr_height = 0;
	for(int i = 0; i < rs.n_threads; ++i) {
		thread_data[i].rs = &rs;
		thread_data[i].thread_i = i;
		thread_data[i].start_height = curr_height;
		thread_data[i].end_height = curr_height + height_per_thread > rs.height ? rs.height : curr_height + height_per_thread;
		curr_height = thread_data[i].end_height;
		assert(pthread_create(&thread_handles[i], NULL, render_thread, (void*)&thread_data[i]) == 0);
		printf("%d %d %d\n", thread_data[i].thread_i, thread_data[i].start_height, rs.height);
	}
	assert(curr_height == rs.height);

	while(1) {
		usleep(250*1000);
		printf("\r");
		int good = true;
		for(int i = 0; i < rs.n_threads; ++i) {
			int total_px = (thread_data[i].end_height - thread_data[i].start_height)*rs.width;
			printf("%%%2.2f ", 100.0*(double)pxcounts[i]/total_px);
			if(pxcounts[i] < total_px) good = false;
		}
		fflush(stdout);
		if(good) break;
	}
	printf("\n");

	for(int i = 0; i < rs.n_threads; ++i) {
		pthread_join(thread_handles[i], NULL);
	}
}

void write(const render_settings_t& rs, const char* filename) {
	FILE* fp = fopen(filename, "w"); assert(fp); DEFER(fclose(fp));
	fprintf(fp, "P3\n%d %d\n255\n", rs.width, rs.height);
	for(int h = rs.height - 1; h >= 0; --h) {
		for(int w = 0; w < rs.width; ++w) {
			vec3<uint8_t>* px = &rs.out_image[h*rs.width + w];
			fprintf(fp, " %d %d %d", px->r, px->g, px->b);
		}
	}
}

int main(int argc, char* argv[]) {
	goto start;
usage:
	fprintf(stderr ,"usage: %s <scene> <X> <Y> <samples> <n_threads> [seed]\n", argv[0]);
	return 1;
start:
	if(argc < 5) {
		goto usage;
	}

	fstream fscene; fscene.open(argv[1], fstream::in);
	if(!fscene.is_open()) {
		fprintf(stderr, "failed to open %s\n", argv[1]);
		goto usage;
	}

	{
		time_t seed; {
			if(argc >= 7) seed = atoi(argv[6]);
			else seed = time(0);
		}
		srand(seed); printf("using seed value %ld.\n", seed);
	}

	render_settings_t rs;
	rs.width = atoi(argv[2]); 
	rs.height = atoi(argv[3]);
	rs.n_samples = atoi(argv[4]);
	rs.n_threads = atoi(argv[5]);
	if(rs.width <= 0 || rs.height <= 0 || rs.n_samples <= 0 || rs.n_threads <= 0) {
		goto usage;
	}

	vec3<uint8_t>* image = (vec3<uint8_t>*)malloc(sizeof(*image)*rs.height*rs.width); 
	double* image_heat = (double*)malloc(sizeof(*image_heat)*rs.height*rs.width); 
	DEFER(free(image); free(image_heat););
	rs.out_heat = image_heat;
	rs.out_image = image;
	{
		v3 cam_from; 
		v3 cam_to;
		double cam_vfov_deg; 
		double cam_aperture; 
		double dist_to_focus;
		if(!(fscene >> cam_from.x >> cam_from.y >> cam_from.z)) goto usage;
		if(!(fscene >> cam_to.x >> cam_to.y >> cam_to.z)) goto usage;
		if(!(fscene >> cam_vfov_deg)) goto usage;
		if(!(fscene >> cam_aperture)) goto usage;
		if(!(fscene >> dist_to_focus)) goto usage; 
		if(dist_to_focus < 0) dist_to_focus = (cam_from - cam_to).len();
		rs.cam = camera_t(
			cam_from,
			cam_to,
			v3(0.0, 1.0, 0.0), // global UP
			rad(cam_vfov_deg), // vertical fov
			(double)(rs.width)/rs.height, // aspect
			cam_aperture, // aperture
			dist_to_focus // dist_to_focus
		);
	}

	double radius; if(!(fscene >> radius)) goto usage;

	int n_materials; if(!(fscene >> n_materials)) goto usage;
	material_t materials[n_materials];
	for(int i = 0; i < n_materials; ++i) {
		static const char* strs[] = { "matte", "metal", "transparent" };
		string w; if(!(fscene >> w)) goto usage;
		int j;
		for(j = 0; j < (int)SIZE(strs); ++j) {
			if(w == strs[j]) break;
		}
		if(j >= (int)SIZE(strs)) goto usage;
		material_t* m = &materials[i];
		switch(j) {
			case 0: {
				m->type = MAT_MATTE;
				if(!(fscene >> m->albedo.x >> m->albedo.y >> m->albedo.z)) goto usage;
			} break;
			case 1: {
				m->type = MAT_METAL;
				if(!(fscene >> m->albedo.x >> m->albedo.y >> m->albedo.z)) goto usage;
				if(!(fscene >> m->fuzz)) goto usage;
			} break;
			case 2: {
				m->type = MAT_TRANSPARENT;
				if(!(fscene >> m->refraction)) goto usage;
			} break;
			default: assert(0);
		}
		// convert from [0, 255]
		m->albedo /= 255.0;
	}

	if(!(fscene >> rs.scene.n) || rs.scene.n <= 0) goto usage;
	++rs.scene.n;
	rs.scene.spheres = (sphere_t*)malloc(sizeof(*rs.scene.spheres)*rs.scene.n);
	DEFER(free(rs.scene.spheres));
	rs.scene.spheres[rs.scene.n - 1] = get_sphere_metal(
		v3(0.0, -1000.0, -1.0), 
		1000.0, 
		tov3(208, 198, 177),
		0.2f
	);

	for(int i = 0; i + 1 < rs.scene.n; ++i) {
		double a, b, c;
		if(!(fscene >> a >> b >> c)) goto usage;

		int material_i = rand()%n_materials;
		rs.scene.spheres[i].center = v3(a, b + radius, c);
		rs.scene.spheres[i].radius = radius;
		rs.scene.spheres[i].material = materials[material_i];
	}
	bvh_node_t* root = NULL;
	if(USE_BVH) {
		bvh_node_t* root = (bvh_node_t*)malloc(sizeof(*root));
		sphere_t* spheres[rs.scene.n];
		for(int i = 0; i < rs.scene.n; ++i) {
			spheres[i] = &rs.scene.spheres[i];
		}
		build_bvh(root, spheres, rs.scene.n);
		rs.root = root;
	}
	DEFER(if(root) { free_bvh(root); });

	printf("scene:\n");
	printf("\t%d spheres\n", rs.scene.n);
	printf("\t%dx%dx%d image\n", rs.width, rs.height, rs.n_samples);
	printf("\t%s lookup\n", rs.root ? (USE_BVH_SAH ? "bvh (sah)" : "bvh") : "linear");
	printf("\t%d threads\n", rs.n_threads);

	double t0; {
		struct timeval tm; gettimeofday(&tm, NULL);
		t0 = (double)tm.tv_sec + (double)tm.tv_usec/1000000.0;
	}
	render(rs);
	double t1; {
		struct timeval tm; gettimeofday(&tm, NULL);
		t1 = (double)tm.tv_sec + (double)tm.tv_usec/1000000.0;
	}

	printf("%lf sec to render.\n", t1 - t0); 
	string out_file = argv[1];
	out_file += "_";
	out_file += to_str(rs.width);
	out_file += "x";
	out_file += to_str(rs.height);
	out_file += "x";
	out_file += to_str(rs.n_samples);
	{
		string out_file_result = out_file;
		out_file_result += ".ppm";
		printf("writing to %s\n", out_file_result.c_str());
		write(rs, out_file_result.c_str());
	}
	if(USE_BVH && USE_BVH_OBJ) {
		string out_file_result = out_file;
		out_file_result += "_bvh";
		if(USE_BVH_SAH) out_file_result += "sah";
		out_file_result += ".obj";
		fstream fout;
		fout.open(out_file_result.c_str(), fstream::out);
		if(fout.is_open()) {
			printf("writing bvh obj to %s\n", out_file_result.c_str());
			int nv = 1;
			write_bvh_obj(rs.root, fout, nv);
		}
	}
	if(USE_HEAT) {
		// convert the heat map to rgb
		double max_heat = -1.0;
		for(int i = 0; i < (int)SIZE(image_heat); ++i) {
			if(max_heat < image_heat[i]) max_heat = image_heat[i];
		}
		max_heat /= 100.0;
		for(int i = 0; i < (int)SIZE(image_heat); ++i) {
			double r = 0.0; 
			double g = 0.0; 
			double b = 0.0;
			double norm = image_heat[i]/max_heat;
			r = min(norm, 1.0);
			image[i].r = (int)(255.99f)*pow(r, 1.0/2.2);
			image[i].g = (int)(255.99f)*pow(g, 1.0/2.2);
			image[i].b = (int)(255.99f)*pow(b, 1.0/2.2);
		}
		string out_file_result = out_file;
		out_file_result += "_heat.ppm";
		printf("writing heatmap to %s\n", out_file_result.c_str());
		rs.out_image = image; // image = heatmap now
		write(rs, out_file_result.c_str());
	}

	return 0;
}
