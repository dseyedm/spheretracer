#ifndef SCOPE_H
#define SCOPE_H

template <typename F>
struct scope_exit_t {
	F f;

	scope_exit_t(F f) : f(f) {}
	~scope_exit_t() { f(); }
};

template <typename F>
inline scope_exit_t<F> make_scope_exit(F f) {
	return scope_exit_t<F>(f);
};

#define STRING_JOIN2(arg1, arg2) DO_STRING_JOIN2(arg1, arg2)
#define DO_STRING_JOIN2(arg1, arg2) arg1 ## arg2
#define DEFER(code) \
	auto STRING_JOIN2(scope_exit_, __LINE__) = make_scope_exit([&](){code;})

#endif
