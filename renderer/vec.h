#ifndef VEC_H
#define VEC_H

#include <cmath> // std::sqrt

#define VEC_INIT_ZERO
template<typename T>
struct vec3 {
	union {
		struct { T x, y, z; };
		struct { T r, g, b; };
		T v[3];
	};

	vec3() {
#ifdef VEC_INIT_ZERO
		x = y = z = 0;
#endif
	}
	vec3(T _x, T _y, T _z) { x = _x; y = _y; z = _z; }
	vec3(T _v)             { x = _v; y = _v; z = _v; }

	inline const vec3& operator + () const { return *this; }
	inline       vec3  operator - () const { return vec3(-x, -y, -z); }
	inline T           operator [](int i) const { return v[i]; }
	inline T&          operator [](int i)       { return v[i]; }

	inline vec3& operator += (const vec3& v) { x += v.x; y += v.y; z += v.z; return *this; }
	inline vec3& operator -= (const vec3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
	inline vec3& operator *= (const vec3& v) { x *= v.x; y *= v.y; z *= v.z; return *this; }
	inline vec3& operator /= (const vec3& v) { x /= v.x; y /= v.y; z /= v.z; return *this; }
	inline vec3& operator *= (const T t)     { x *=   t; y *=   t; z *=   t; return *this; }
	inline vec3& operator /= (const T t)     { x /=   t; y /=   t; z /=   t; return *this; }

	inline T len2() const { return x*x + y*y + z*z; }
	inline T len()  const { return std::sqrt(x*x + y*y + z*z); }

	inline vec3<T> unit() const { T l = len(); return vec3<T>(x/l, y/l, z/l); }
	inline void make_unit() { T l = len(); x /= l; y /= l; z /= l; }
};
template<typename T> inline vec3<T> operator + (const vec3<T>& v0, const vec3<T>& v1) { return vec3<T>(v0.x + v1.x, v0.y + v1.y, v0.z + v1.z); }
template<typename T> inline vec3<T> operator - (const vec3<T>& v0, const vec3<T>& v1) { return vec3<T>(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z); }
template<typename T> inline vec3<T> operator * (const vec3<T>& v0, const vec3<T>& v1) { return vec3<T>(v0.x * v1.x, v0.y * v1.y, v0.z * v1.z); }
template<typename T> inline vec3<T> operator / (const vec3<T>& v0, const vec3<T>& v1) { return vec3<T>(v0.x / v1.x, v0.y / v1.y, v0.z / v1.z); }
template<typename T> inline vec3<T> operator * (T t, const vec3<T>& v) { return vec3<T>(t*v.x, t*v.y, t*v.z); }
template<typename T> inline vec3<T> operator * (const vec3<T>& v, T t) { return vec3<T>(t*v.x, t*v.y, t*v.z); }
template<typename T> inline vec3<T> operator / (const vec3<T>& v, T t) { return vec3<T>(v.x/t, v.y/t, v.z/t); }
template<typename T> inline T dot(const vec3<T>& v0, const vec3<T>& v1) { return v0.x*v1.x + v0.y*v1.y + v0.z*v1.z; }
template<typename T> inline vec3<T> cross(const vec3<T>& v0, const vec3<T>& v1) {
	return vec3<T>(
		  v0.y*v1.z - v0.z*v1.y,
		-(v0.x*v1.z - v0.z*v1.x),
		  v0.x*v1.y - v0.y*v1.x
	);
}
template<typename T> inline vec3<T> lerp(const vec3<T>& v0, T t, const vec3<T>& v1) {
	return (1.0f - t)*v0 + t*v1;
}

typedef vec3<double> v3;
typedef vec3<double> vec3f;

#endif
